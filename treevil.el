;;; treevil.el --- Emacs + tree-sitter + evil        -*- lexical-binding: t; -*-

;; Copyright © 2022 Hraban Luyat

;; Author: Hraban Luyat <hraban@0brg.net>
;; Keywords: lisp
;; Version: 0.0.1

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, version 3 of the License.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Playground for structural editing using Emacs, Evil and Treesitter

;; Usage: M-x eval-buffer this file, and make sure tree-sitter-mode is loaded
;; in your target buffer. E.g. by calling M-x global-tree-sitter-mode.


;;; Code:


(require 'cl-lib)
(require 'dash)
;; Do I actually need to require this? Don't think so, right? That's up to the user afaik?
(require 'tree-sitter)

(defun h/get-params-at-pos ()
  "Get the params for the function definition at pos"
  ;; TODO: adapt per language. Either a single smart function, or make the
  ;; whole thing per lang. E.g. C uses function_definition, not _declaration
  ;; (because that's something else), and its parameters aren't a first class
  ;; child of the definition node. How general do we wanna make this?
  (-> 'function_declaration
      tree-sitter-node-at-pos
      (tsc-get-child-by-field :parameters)))

;; I'm starting to understand the danger of lisp
(defun h/with-bind-to-pcase-bind (with-bind-form)
  "Macro process a single entry in a pcase-let bind form list"
  (cl-destructuring-bind ((start-sym end-sym) node-form) with-bind-form
    ;; I know. ,,_,,
    `(`[,,start-sym ,,end-sym] (or (-some-> ,node-form tsc-node-range) [nil nil]))))

(defmacro h/with-node-bounds (with-bind-forms &rest body)
  (if (not (listp (caar with-bind-forms)))
      ;; Convenient short version: only one bind form, flattened to
      ;; top-level. Handle recursively.
      `(h/with-node-bounds (,with-bind-forms)
         ,@body)
    ;; Official case: fully nested
    (let ((let-bind-forms (mapcar 'h/with-bind-to-pcase-bind with-bind-forms)))
      `(pcase-let* ,let-bind-forms
         ,@body))))

(defun h/add-arg (new-arg)
  (interactive "sadd arg: ")
  (save-excursion
    (let* ((params (h/get-params-at-pos))
           (numargs (tsc-count-named-children params)))
      (h/with-node-bounds (((start end) params))
        (if (= 0 numargs)
            ;; No existing args: easy.
            (progn
              (goto-char start)
              (forward-char)
              (insert new-arg))
          ;; There are args.
          ;;
          ;; 1. Go to the end of the last arg
          ;; 2. add a comma
          ;; 3. get whatever is between the opening-paren and the first arg
          ;;    3a. if that is non-empty: insert it here
          ;;    3b. if it is empty: insert one space
          ;; 4. add the new arg
          ;;
          ;; This handles args on one line vs args split up with newlines, and
          ;; potential trailing commas.
          (h/with-node-bounds (((paren-start paren-end) (tsc-get-nth-child params 0))
                               ((arg1-start arg1-end) (tsc-get-nth-named-child params 0))
                               ((argn-start argn-end) (->> numargs 1- (tsc-get-nth-named-child params))))
            (let ((between (tsc--buffer-substring-no-properties paren-end arg1-start)))
              (goto-char argn-end)
              (insert ",")
              (insert (if (string= "" between) " " between))
              (insert new-arg))))))))

(cl-defun h/get-param (inner-p)
  "Get bounds for the param at point using treesitter.

This can be used as a object movement function in evil.  I'm not sure about the
evil API but this seems to work for now. Probably missing lots of edge cases.

E.g. I have no idea what the 'inclusive symbol is for. It also doesn't work with
selection (v<movement>). All fixable, I'm sure.
"
  (let* ((arg (tree-sitter-node-at-pos 'required_parameter))
         (prv (tsc-get-prev-named-sibling arg))
         (nxt (tsc-get-next-named-sibling arg)))
    (when arg
      (h/with-node-bounds (((prv-start prv-end) prv)
                           ((arg-start arg-end) arg)
                           ((nxt-start nxt-end) nxt))
        (cond
         ;; Easy: just the arg
         (inner-p (list arg-start arg-end 'inclusive))
         ;; Ok, outer requested. Try to get some noise along with us.  Do we
         ;; have a param following us? Slurp upto there.
         (nxt (list arg-start nxt-start 'inclusive))
         ;; No next arg. Is there a previous one? Slurp from there.
         (prv (list prv-end arg-end 'inclusive))
         ;; Lol we were the only arg all along :)
         (t (list arg-start arg-end 'inclusive)))))))

(defun h/i-param ()
  (interactive)
  (h/get-param t))

(defun h/a-param ()
  (interactive)
  (h/get-param nil))

(defun h/tree-sitter-mode-p ()
  (bound-and-true-p tree-sitter-mode))

(defun h/only-in-tree-sitter-mode (f)
  "Turn command F into a NOP in buffers without tree-sitter-mode loaded"
  (lambda (&rest args)
    (interactive)
    (when (h/tree-sitter-mode-p)
      (apply f args))))

(defun h/register-keys ()
  ;; Just mapping whatever atm.

  ;; This is not ideal: it maps these keys _globally_, because these maps are
  ;; shared by all evil buffers. Meaning non-treemacs buffers now also get these
  ;; keys. Googling this suggests there is no easy way to hook something into a
  ;; specific map for a buffer only; that's kinda the point of the local map
  ;; itself, so if you don't wanna use that, you're global by definition. TODO:
  ;; should I just hook this into the local map with ia and aa? Option 2: keep
  ;; this map, but detect absence of treemacs mode and just bail if not
  ;; loaded. That's the easiest so it's what I do now. But also ugly.
  (define-key evil-inner-text-objects-map "a" (h/only-in-tree-sitter-mode 'h/i-param))
  (define-key evil-outer-text-objects-map "a" (h/only-in-tree-sitter-mode 'h/a-param))
  (keymap-local-set "s-t" (h/only-in-tree-sitter-mode 'h/add-param)))

;; There's no reason to gate this behind the tree-sitter-mode-hook again, but
;; whatever. It's one step in the right direction I guess.
(add-hook 'tree-sitter-mode-hook 'h/register-keys)

(provide 'treevil)

;;; treevil.el ends here
